syntax on
colorscheme desert

set number
set rnu
set showmode

set expandtab
set smarttab
set shiftwidth=4
set softtabstop=4
set tabstop=4

set autoindent
set smartindent

set encoding=utf-8
set mouse=a
set backspace=indent,eol,start
set noswapfile
set incsearch
